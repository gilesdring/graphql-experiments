import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class Context {
  @Field({ nullable: true })
  name?: string;
}

@ObjectType()
export class Hat {
  @Field()
  name: string;

  @Field(() => Context)
  context: Context;
}

@ObjectType()
export class Person {
  @Field()
  name: string;

  @Field(() => [Hat])
  hats: Hat[];
}
