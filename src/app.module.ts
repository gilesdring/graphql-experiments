import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { GraphDbService } from './graph-db/graph-db.service';
import { PersonResolver } from './graphql/person.resolver';
import { ProfileResolver } from 'src/person/profile.resolver';
import { HasRoleDirective } from 'src/directives';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      schemaDirectives: {
        hasRole: HasRoleDirective,
      },
    }),
  ],
  providers: [GraphDbService, PersonResolver, ProfileResolver],
})
export class AppModule {}
